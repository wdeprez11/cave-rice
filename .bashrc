#!/bin/bash

set -o vi #Enable vi mode
stty -ixon # Disable Ctrl-S and Ctrl-Q functionality
shopt -s autocd # Type dir name to change directory
HISTSIZE= HISTFILESIZE= # Infinite Bash history

# System wide
alias p='sudo pacman'
alias ss='sudo systemctl'
alias sdn='sudo shutdown now'

alias rss='newsboat'
alias scrot='scrot -q 100 ~/mount/hdd1/media/pictures/scrots/%Y-%m-%d-%T-screenshot.png'
alias ls='ls --color=auto'
alias df='df -ah'
alias v='vim'
alias fm='vifm .'
alias mu='ncmpcpp'
alias q='exit'
alias def='sdcv'
alias ki='killall -9'
alias del='rm -rf'

#─╼

PROMPT_COMMAND="
PS1='\[$(tput sgr0)\]'
PS1+='\[$(tput setaf 1)\]'
PS1+='['
PS1+='\[$(tput setaf 3)\]'
PS1+='\u'
PS1+='\[$(tput setaf 2)\]'
PS1+='@'
PS1+='\[$(tput setaf 6)\]'
PS1+='\h'
PS1+='\[$(tput sgr0)\]'
PS1+=' '
PS1+='\[$(tput setaf 5)\]'
PS1+='\w'
PS1+='\[$(tput setaf 1)\]'
PS1+=']'
PS1+='\[$(tput sgr0)\]'
PS1+='\$ '
"

# echo -e "Word of the day!\n\n\nDon't give them headspace, be productive!\n\n$(calcurse -t)\n"
