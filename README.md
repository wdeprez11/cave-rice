# Cave Rice

File sharing & Backups

## System Relative
+ arch
+ bspwm
+ lemonbar
+ sxhkd
+ dunst
+ urlview
+ cronie
+ st
+ compton
+ entr
+ bash
+ docker

## General Programs
+ qutebrowser
+ ncmpcpp, mpd, & mpc
+ newsboat
+ zathura
+ vifm
+ mpv
+ calcurse
+ sc-im
+ feh
+ vim
+ visual-studio-code
