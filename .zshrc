# The following lines were added by compinstall
zstyle :compinstall filename '/home/wd/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
autoload -U colors && colors
# PS1='[%n@%m%f %~]%# '

PS1='%F{9}[%f%F{11}%n%f%F{10}@%f%F{12}%m%f %F{13}%~%f%F{9}]%f%# '

alias vim='nvim'
alias vi='nvim'
alias v='nvim'

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPS="--extended"
