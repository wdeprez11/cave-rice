#!/bin/bash

export PATH="$PATH:$HOME/mount/hdd1/work/scripts:$HOME/.local/bin"

export FILEMAN="vifm"
export EDITOR="vim"
export PAGER="most"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export MUSIC="mpd"

# export WIDTH=$(xrandr | grep '*' | awk '{print $1}' | sed 's/[x].*//' | head -n 1)

if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    exec startx;
fi

sudo -n loadkeys $HOME/.scripts/ttymaps.kmap 2>/dev/null
# wal -Rn >/dev/null
