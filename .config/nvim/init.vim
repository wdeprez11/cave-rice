" 'init.vim' -- vimrc
" Description: Initial startup commands for neovim

let mapleader = ","
set encoding=UTF-8

" Change Behaviors: {{{
set wildmode=longest,list,full " Text-completion
setlocal foldmethod=syntax     " Folding
set hlsearch                   " Highlight searched words
set incsearch                  " Search whilst typing
set splitbelow splitright      " Don't move panes weirdly
" }}}

" Tabs: {{{
set vb t_vb=
set ts=4 sts=4 sw=4 expandtab
" }}}

" UI Appearance: {{{

" Syntax: {{{
syntax on
colorscheme Theme
set t_Co=256
set showmatch
" }}}

" Status: {{{
set showmode
source ~/.config/nvim/statusline.vim
" }}}

" VisibleEOL: {{{
set list
set listchars=tab:▸\ ,eol:¬
highlight ExtraWhitespace ctermbg=131
highlight NonText ctermfg=8 ctermbg=none
" End of file tildes & end of line return character
" }}}

" Splits: {{{
set fillchars+=vert:\|
highlight vertsplit ctermfg=8 ctermbg=8
" }}}

" Ruler: {{{
set number
set relativenumber
set ruler

" Permanent Gutter: {{{
autocmd BufRead,BufNewFile * setlocal signcolumn=yes
autocmd FileType tagbar,nerdtree setlocal signcolumn=no
" }}}
" }}}

" Cursor: {{{
"let &t_SI = "\e[5 q"
"let &t_EI = "\e[1 q"
set ttimeoutlen=0
set modeline
set mouse=a
"  set mouse=
set cursorline

if exists('$TMUX')
    let &t_SI = "\ePtmux;\e\e[5 q\e\\"
    let &t_EI = "\ePtmux;\e\e[2 q\e\\"
else
    let &t_SI = "\e[5 q"
    let &t_EI = "\e[1 q"
endif
" }}}
" }}}

" Filetype: {{{
filetype plugin on
filetype on
" }}}

" Plugins: {{{
if has('nvim')
    " Call: {{{
    call plug#begin('~/.config/nvim/plugged')
    " Deoplete: {{{
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    Plug 'Shougo/neoinclude.vim'
    Plug 'Shougo/deoplete-clangx'
    Plug 'deoplete-plugins/deoplete-go'
    Plug 'deoplete-plugins/deoplete-jedi'
    Plug 'Shougo/neco-vim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
    " }}}

    " Linting And Highlighting: {{{
    Plug 'sheerun/vim-polyglot'
    Plug 'w0rp/ale'
    Plug 'octol/vim-cpp-enhanced-highlight'
    " }}}

    " Git: {{{
    Plug 'jreybert/vimagit'
    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'
    " }}}

    " UI Navigation: {{{
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim'
    Plug 'scrooloose/nerdtree'
    Plug 'christoomey/vim-tmux-navigator'
    " }}}

    " Appearance: {{{
    Plug 'Chiel92/vim-autoformat'
    Plug 'yggdroot/indentline'
    Plug 'google/vim-searchindex'
    "Plug 'mhinz/vim-startify' " Kinda cool -- keep an eye on -- use once more robust
    " }}}

    " InText Navigation: {{{
    Plug 'scrooloose/nerdcommenter'
    Plug 'terryma/vim-multiple-cursors'
    Plug 'tpope/vim-surround'
    Plug 'Raimondi/delimitMate'
    Plug 'justinmk/vim-sneak' " New way to jump around
    " }}}

    " Other: {{{
    Plug 'vimwiki/vimwiki'
    " }}}
    call plug#end()
    " }}}

    " Configs: {{{
    let NERDTreeShowHidden=1 " Show hidden files
    "let g:deoplete#enable_at_startup = 1
    "let g:deoplete#disable_auto_complete = 1

    "call deoplete#custom#option({
    "\ 'auto_complete_delay': 0,
    "\ 'smart_case': v:true,
    "\ })
    let g:indentLine_color_term = 7
    let g:indentLine_setColors = 7
    let g:indentLine_char_list = ['|', '¦', '┆', '┊']
    " }}}
endif
" }}}

"   Split movement
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"   Refresh
map <leader>r :so ~/.config/nvim/init.vim \| Autoformat<CR>

"   Spell-check | GUI FAGS BTFO | orthography
map <leader>o :setlocal spell! spelllang=en_us<CR>

"   Bibliography | Need to read into this more
map <leader>b :vsp<space>$BIB<CR>

"   URLView
map <leader>u :w<Home> silent <End> !urlview<CR>

"   Compile doc
map <leader>c :w! \| !compiler <c-r>%<CR><CR>

"   Open relative .pdf, .html, etc......
map <leader>p :!opout <c-r>%<CR><CR>

"   NERDTree toggle
map <leader>b :NERDTreeToggle<CR>

"   Toggle Pasting
set pastetoggle=<leader>v

"   GTFO Ugly Shidd!!
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
autocmd VimLeave *.tex !texclear %

"   Delete Trailing Whitespace
autocmd BufWritePre * %s/\s\+$//e

"   Format Code
autocmd BufWrite * :Autoformat

"   INSERT macros
"inoremap " ""<left>
"inoremap ' ''<left>

"inoremap ( ()<left>
"inoremap () ()
"inoremap (<CR> (<CR>)<ESC>O
"inoremap (;<CR> (<CR>);<ESC>O
"inoremap (,<CR> (<CR>),<ESC>O

"inoremap { {}<left>
"inoremap {} {}
"inoremap {<CR> {<CR>}<ESC>O
"inoremap {;<CR> {<CR>};<ESC>O
"inoremap {,<CR> {<CR>},<ESC>O

"inoremap [ []<left>
"inoremap [] []
"inoremap [<CR> [<CR>]<ESC>O
"inoremap [;<CR> [<CR>];<ESC>O
"inoremap [,<CR> [<CR>],<ESC>O
